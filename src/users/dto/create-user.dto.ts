import { ApiModelProperty } from '@nestjs/swagger';
import { IsInt, IsString, Max, Min } from 'class-validator';
export class CreateUserDto {
  @IsString()
  @ApiModelProperty()
  public readonly name: string;
  @IsInt()
  @Min(0)
  @Max(150)
  @ApiModelProperty()
  public readonly age: number;
}
