import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './interfaces/user.interface';

@Injectable()
export class UsersService {
  constructor(@Inject('USER_MODEL') private readonly userModel: Model<User>) {}

  public async getUsers(): Promise<ReadonlyArray<User>> {
    return await this.userModel.find().exec();
  }

  public getUser(id: string): Promise<any> {
    return Promise.resolve({});
  }

  public async createUser(createUserDto: CreateUserDto): Promise<User> {
    const user = new this.userModel(createUserDto);
    return await user.save();
  }
}
