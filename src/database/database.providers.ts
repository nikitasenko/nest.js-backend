import * as mongoose from 'mongoose';

export const databaseProviders: ReadonlyArray<any> = [
  {
    provide: 'DATABASE_CONNECTION',
    useFactory: async (): Promise<typeof mongoose> =>
      // tslint:disable-next-line
      await mongoose.connect(process.env.MONGO_URL || 'mongodb://localhost/nest'),
  },
];
